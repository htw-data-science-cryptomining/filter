package ds;

// https://github.com/uBlockOrigin/uAssets/blob/master/filters/resource-abuse.txt

/**
 * miner key words in alphabetical order
 */

public enum AbuseList {
    cnhv,
    coin,
    crypto,
    edgemesh,
    edgeno,
    hive,
    jsecoin,
    mine,
    monero,
    oload,
    poi,
    webmine;

    static boolean abusePresence(String in){
        boolean result = false;
        for(AbuseList temp : AbuseList.values())
            if (in.toLowerCase().contains(temp.toString().toLowerCase())) {
                System.out.println("Found match: " + in);
                return true;
            }
        return result;
    }
}
