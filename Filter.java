package ds;

import java.io.*;
import java.util.*;

import org.json.simple.JSONObject;
import org.json.simple.parser.*;
import org.json.simple.JSONArray;

//http://www.geeksforgeeks.org/parse-json-java/
class Filter {

    //known miner in abc order
    static String[] abuseList = {
        "cnhv",
        "coin",
        "crypto",
        "edgemesh",
        "edgeno",
        "hive",
        "jsecoin",
        "mine",
        "monero",
        "oload",
        "poi",
        "webmine"
    };

    static boolean abusePresence(String in){
        for(String temp : abuseList)
            if (in.toLowerCase().contains(temp.toLowerCase())) {
                //System.out.println("Found match: " + in);
                return true;
            }
        return false;
    }

    public static void main(String[] args) {
        String path = "d:/0T/miner";
        //System.out.printf("Start iterator in [%s]\n", path);
        File f = new File(path);

        try {
            System.out.println(Arrays.toString(f.listFiles()));
            iterator(f.listFiles());
        } catch (Exception e) {
            System.out.println("Exception in main: " + e);
            e.printStackTrace();
        }
    }

    static void iterator(File[] allFiles) throws Exception {
        Set<String> processedDirs = new HashSet<>();

        //iterate content of path
        for (File temp : allFiles) {
            if (temp.isDirectory()) {
                System.out.println("\tDIR: " + temp.getName());

                // Calls same method again
                iterator(temp.listFiles());
            }

            //inside a dir with json & js, forward it to the filter
            else {
                String dirPath = temp.getParent();
                //System.out.println(temp.getName());

                if(!processedDirs.contains(dirPath)) {
                    System.out.printf("Start filter in [%s]\n", dirPath);
                    //System.out.println(Arrays.toString(new File(dirPath).listFiles()));
                    filter(new File(dirPath).listFiles());
                }
                processedDirs.add(dirPath);
            }
        }
    }

    static void filterTest (File[] dirFiles) throws Exception {
        //System.out.println("Start filter");
        for(File temp : dirFiles)
            System.out.println(temp.getName());
        System.out.println("End filter");
    }

    static void filter (File[] dirFiles) throws Exception {
        String inName = "NotFound", outName;

        //json that will be written to the output
        JSONObject out = new JSONObject();

        // process all the files in one dir
        for(File temp : dirFiles) {
            int counter = 0;

            //copy metas if json
            if(temp.getName().endsWith(".json")) {
                inName = temp.getName();

                // parsing .json file to an object
                Object o = new JSONParser().parse(new FileReader(temp.getAbsolutePath()));
                JSONObject in = (JSONObject) o;

                // copy meta-data to output: paragraph = array name, values
                out.put("meta", in.get("meta"));
            }

            //scan java scripts for miner
            if (temp.getName().endsWith(".js")){

                //map to assemble miner occurrences
                Map m = new LinkedHashMap();
                String line;
                BufferedReader in = new BufferedReader(new FileReader(temp.getAbsolutePath()));
                //m.put(Integer.toString(counter++), temp.getName());

                //line for line
                while((line = in.readLine()) != null) {
                    //System.out.println(line);
                    if (abusePresence(line)) {
                        //String scriptName = Integer.toString(counter).concat(temp.getName());
                        //System.out.println(temp.getName());
                        m.put(Integer.toString(counter++), line);
                    }
                }

                //add map to the output json
                out.put("script", m);
            }
        }

        outName = "OUT_".concat(inName);
        System.out.println("json name: " + outName);
        //System.out.println("meta: " + out.get("meta"));
        System.out.println("js: " + out.get("script"));

//        System.out.printf("json name: %s\n" +
//                        "meta: %s\n" +
//                "js: %s\n", outName, out.get("meta"), out.get("script"));

//        // writing output JSON to file
//        PrintWriter pw = new PrintWriter("jOUT.json");
//        pw.write(out.toJSONString());
//
//        pw.flush();
//        pw.close();
    }

    // reduce the link length to 2 levels after the slash
    static String linkCheck(String in) {
        System.out.println("Original: " + in);
        String[] split = in.split("/{1,2}");
        String s = split[0] + "//";
/*
        for(String temp : split)
            System.out.println(temp);

        System.out.println();
*/
        s = split[0] + "//";
        for(int i = 1; i < 3; i++)
            s = s + split[i] + "/";

        System.out.println("Output: " + s);
        return s;
    }
}