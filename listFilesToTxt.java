/**
 * Created by <kuzne4eg@gmail.com> on 05.12.2017
 */

import java.io.*;
import java.util.Calendar;

public class listFilesToTxt {
    public static void main(String[] args) {
        BufferedWriter bw;
//        String path = args[0];
        String path = "d:/0T/miner";
        File f = new File(path);
        File[] fileList = f.listFiles();
        int size = 0;

        if (fileList.length > 0) {
            size = fileList.length;
            //System.out.println(size);
        }
        else {
            System.err.println("Failed to determine file list size!");
        }

//        d:\0T\miner\0x00sec.org
//        d:\0T\miner\3ds-models.org
//        d:\0T\miner\booksonline.com.ua
//        d:\0T\miner\ifolki.com

        /*//arraycopy( Object src, int srcPos, Object dest, int destPos, int length )
        int removeInd = 0;
        System.arraycopy(fileList, removeInd + 1, fileList, 0, fileList.length - 1 - removeInd);
        System.out.println(size);
        System.out.println(Calendar.getInstance().getTime());*/

        // actual writing
        try {
            bw = new BufferedWriter(new FileWriter("listFiles.txt"));

            bw.write(Calendar.getInstance().getTime().toString());
            bw.newLine();
            bw.write(Integer.toString(size));
            bw.newLine();

            for (int i = 0; i < size; i++) {
                bw.write(fileList[i].toString());
                bw.newLine();
                System.out.println(fileList[i]);
            }
            bw.close();
        } catch (IOException e) {
            System.out.println("Fehler beim Erstellen der Datei");
        }
    }
}